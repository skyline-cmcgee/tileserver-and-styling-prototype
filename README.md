# Tile Server and Custom Styles

This project serves as a development environment and R&D for tile server and styles development.

## Table of Contents
- [High Level Solution](#high-level-solution)
- [Prototype](#prototype)
    - [Pre-requisites](#pre-requisites)
    - [Usage](#usage)
        - [Get tiles and start tile server](#get-tiles-and-start-tile-server)
    - [tilemaker](#tilemaker)
- [References](#references)

## High Level Solution

Tile Data
1. Intialize data by Downloading OSM data from [Planet OSM](https://planet.openstreetmap.org/) or [Geofabrik](https://download.geofabrik.de/).
2. Process data and convert to mbtiles.
3. Update on a cron using changesets or doing a full download and reprocessing.

Tile Server
1. Use [TileServer GL](https://github.com/maptiler/tileserver-gl) with tiles processed from above.
2. Configure appropriate styles for tile server.

## Prototype

### Pre-requisites

1. `docker-compose build`
    * to build `tilemaker`
2. `apt install osmctools`
    * required to convert OSM files to PBF (Protocol Binary Format).

### Usage

#### Get tiles and start tile server

1. Go to [Openstreetmap export tool](https://www.openstreetmap.org/export#map=15/39.2859/-76.6091)
2. Find area that you are interested in on map.
3. Click "Export" button to download OSM for that area.
4. Convert to PBF for `tile-maker`
    * `osmconvert data/FILE.osm --out-pbf > dist/FILE.osm.pbf`
5. Process tiles with `tile-maker` to generate mbtiles.
    * `docker-compose run tile-maker --input dist/FILE.osm.pbf --output dist/FILE.mbtiles --process /resources/process-openmaptiles.lua --config /resources/config-openmaptiles.json`
6. Start tile server
    * `docker-compose run --rm -it --service-ports tileserver-gl`
        * [http://localhost:8080](http://localhost:8080)
            * Note: Raster is currently broken to support styling gui below.
7. **Optional** Start tile styling development server/GUI
    * `./bin/maputnik --watch --file tileserver-gl/styles/skyline/style.json` (binary is for linux, see [Maputnik-CLI](https://github.com/maputnik/editor/wiki/Maputnik-CLI) for other versions)
        * [http://localhost:8000](http://localhost:8000)
        * Note: Changing styles using the editor **WILL** change style.json source file.

### tilemaker

Note tilemaker was cloned AND **EDITED** from [https://github.com/systemed/tilemaker](https://github.com/systemed/tilemaker).

    * see [process-openmaptiles.lua](tilemaker/resources/process-openmaptiles.lua) line 13 for the edit.

#### Important files
* [process-openmaptiles.lua](tilemaker/resources/process-openmaptiles.lua)
    * This file is used to generate layers from OSM data.
* [config-openmaptiles.json](tilemaker/resources/config-openmaptiles.json)
    * High level configuration of tilemaker and the layers [CONFIGURATION.md](https://github.com/systemed/tilemaker/blob/master/docs/CONFIGURATION.md).


## HOWTOS

### Convert osm.pbf to osm (xml)

```bash
osmconvert FILE.osm.pbf --out-osm > FILE.osm
```

**Example:** `osmconvert baltimore.osm.pbf --out-osm > baltimore.osm`


### Applying updates

**Example:**
```bash
osmupdate baltimore.osm.pbf new_baltimore.osm.pbf -b=-76.7115,39.1972,-76.5294,39.3722 --base-url=download.geofabrik.de/north-america/us/maryland-updates
```


## References
* [Geofabrik](https://download.geofabrik.de/) - OpenStreetMap data extracts **updated daily**
    * 13.0 GB - [North America](https://download.geofabrik.de/north-america.html)
* [Planet OSM](https://planet.openstreetmap.org/)
    * 133 GB For entire planet
    * 6.1 GB For latest weekly changeset
* [Openstreetmap export tool](https://www.openstreetmap.org/export#map=15/39.2859/-76.6091) - Smaller controlled datasets useful for development/testing.
* [osmconvert](https://wiki.openstreetmap.org/wiki/Osmconvert) - Tool for converting OSM files between different formats.
* [tilemaker](https://github.com/systemed/tilemaker) - Tilemaker creates vector tiles (in Mapbox Vector Tile format) from an .osm.pbf planet extract, as typically downloaded from providers like Geofabrik. It aims to be 'stack-free': you need no database and there is only one executable to install.
* [TileServer GL](https://github.com/maptiler/tileserver-gl) - Vector and raster maps with GL styles. Server side rendering by Mapbox GL Native. Map tile server for Mapbox GL JS, Android, iOS, Leaflet, OpenLayers, GIS via WMTS, etc.
* [Maputnik](https://github.com/maputnik/editor) - Maputnik is a free and open visual editor for the [Mapbox GL](https://www.mapbox.com/mapbox-gl-js/api/) style specification.
