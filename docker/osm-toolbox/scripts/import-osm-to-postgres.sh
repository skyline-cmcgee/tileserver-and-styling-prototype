#!/bin/bash

# Set the path to the OSM PBF file
OSM_FILE="/data/baltimore.osm.pbf"

# Set the PostgreSQL database connection details
DB_HOST="postgis"
DB_PORT="5432"
DB_NAME="openmaptiles"
DB_USER="openmaptiles"

# Set the table prefix for the imported data
TABLE_PREFIX="baltimore"

# Run osm2pgsql to import the OSM data into the database
  --tag-transform-script /carto/openstreetmap-carto/openstreetmap-carto.lua \
  -C 2500 --number-processes 1 -S /carto/openstreetmap-carto/openstreetmap-carto.style \
  -d "$DB_NAME" -H "$DB_HOST" -P "$DB_PORT" -U "$DB_USER" -W \
  "$OSM_FILE"