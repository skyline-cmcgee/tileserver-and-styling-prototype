#!/bin/bash

# Define the file path
file_path="/data/maryland.osm.pbf"

# Check if the file exists
if [ -f "$file_path" ]; then
  echo "File already exists: $file_path"
else
  # Download Maryland OSM file
  wget https://download.geofabrik.de/north-america/us/maryland-latest.osm.pbf -O "$file_path"
fi