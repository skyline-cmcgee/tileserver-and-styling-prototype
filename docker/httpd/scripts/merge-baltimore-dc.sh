#!/bin/bash

# Merge baltimore.osm.pbf and dc.osm.pbf
osmium merge /data/baltimore.osm.pbf /data/dc.osm.pbf -o /data/merged.osm.pbf
