#!/bin/bash

# This script downloads osm for a given a given region.

# Define the URL based on the flag
if [[ "$1" == "--maryland" ]]; then
  region="maryland"
  url="https://download.geofabrik.de/north-america/us/maryland-latest.osm.pbf"
elif [[ "$1" == "--dc" ]]; then
  region="dc"
  url="https://download.geofabrik.de/north-america/us/district-of-columbia-latest.osm.pbf"
elif [[ "$1" == "--us" ]]; then
  region="us"
  url="https://download.geofabrik.de/north-america/us-latest.osm.pbf"
else
  echo "Invalid flag. Please use --maryland, --dc or --us."
  exit 1
fi

# Define the file path
file_path="/data/$region.osm.pbf"

# Check if the file exists
if [ -f "$file_path" ]; then
  echo "File already exists: $file_path"
else
  wget "$url" -O "$file_path"
fi