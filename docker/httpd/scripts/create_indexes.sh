#!/bin/bash

# this script is used to initialize indexes if using postgis with osm Carto styling

psql -h postgis -U openmaptiles -W -d openmaptiles -a -f /carto/openstreetmap-carto/indexes.sql
