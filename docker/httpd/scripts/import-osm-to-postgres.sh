#!/bin/bash

# this script imports osm data into postgis database

# Set the default value for OSM_FILE
OSM_FILE=""
OSM_CREATE_OR_APPEND="--create"

# Parse command line arguments
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    --file)
      OSM_FILE="$2"
      shift
      shift
      ;;
    --create)
      OSM_CREATE_OR_APPEND="--create"
      shift
      ;;
    --append)
      OSM_CREATE_OR_APPEND="--append"
      shift
      ;;
    *)
      echo "Unknown option: $key"
      exit 1
      ;;
  esac
done

# Check if OSM_FILE is empty
if [[ -z $OSM_FILE ]]; then
  echo "Missing required argument: --file"
  exit 1
fi

# Set the PostgreSQL database connection details
DB_HOST="postgis"
DB_PORT="5432"
DB_NAME="openmaptiles"
DB_USER="openmaptiles"
NUM_PROCESSES=$(nproc)

# Run osm2pgsql to import the OSM data into the database
osm2pgsql -r pbf $OSM_CREATE_OR_APPEND --slim -G --hstore \
  --tag-transform-script /carto/openstreetmap-carto/openstreetmap-carto.lua \
  -C 2500 --number-processes $NUM_PROCESSES -S /carto/openstreetmap-carto/openstreetmap-carto.style \
  -d "$DB_NAME" -H "$DB_HOST" -P "$DB_PORT" -U "$DB_USER" -W \
  "$OSM_FILE"