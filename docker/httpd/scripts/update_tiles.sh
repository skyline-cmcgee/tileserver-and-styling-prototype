#!/bin/bash

# this script updates postgis database automatically with osm updates

DB_HOST="postgis"
DB_PORT="5432"
DB_NAME="openmaptiles"
DB_USER="openmaptiles"
# limited by number of clients allowed to postgres
NUM_PROCESSES=4

# for password protected db
export PGPASSWORD=openmaptiles

osm2pgsql-replication update -d "$DB_NAME" -H "$DB_HOST" -P "$DB_PORT" -U "$DB_USER" \
  --post-processing /app/expire_tiles.sh \
  --max-diff-size 10  --  -G --hstore \
  --tag-transform-script /carto/openstreetmap-carto/openstreetmap-carto.lua \
  -C 2500 --number-processes 4 -S /carto/openstreetmap-carto/openstreetmap-carto.style \
  --expire-tiles=1-20 --expire-output=/var/cache/renderd/dirty_tiles.txt