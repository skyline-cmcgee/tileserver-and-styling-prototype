#!/bin/bash

# this script extracts baltimore from maryland.osm.pbf file (makes it easier to work with due to smaller file/region size)

if [ -f /data/maryland.osm.pbf ]; then
  osmium extract -b -76.7115,39.1972,-76.5294,39.3722 /data/maryland.osm.pbf -o /data/baltimore.osm.pbf
else
  echo "File 'maryland.osm.pbf' does not exist."
fi