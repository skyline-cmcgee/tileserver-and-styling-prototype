
1. `docker-compose -f docker-compose.tile-server.yml up`
    * restart of required of httpd service because postgis takes some time to start up and initialize the first time
2. `docker exec -it alchemy-map-prototype-httpd-1 bash`
3. `./import-osm-to-postgres.sh --create --file /data/maryland.osm.pbf`
    * password is `openmaptiles`
4. `./create_indexes.sh`
5. `cd /carto/openstreetmap-carto`
6. `./scripts/get-external-data.py`
7. FONTS already installed in docker image
8. `carto project.mml > mapnik.xml`


TODO:
[] create sections that explain what files are for configs (config, scripts, mapnik.xml/generated)
[] owner/permissions
[] name the map something else over `example-map`
[] make script for init and include: export PGPASSWORD=openmaptiles





Development: Use a single state for dev/testing.
Latest OSM: https://download.geofabrik.de/north-america/us/
Example: https://download.geofabrik.de/north-america/us/maryland-latest.osm.pbf (310M)

OSC (Changesets)
https://download.geofabrik.de/north-america/us/maryland-updates/000/003/
Example: https://download.geofabrik.de/north-america/us/maryland-updates/000/003/932.osc.gz (108K)



MIT License
Docs: https://github.com/openmaptiles/openmaptiles-tools/tree/master/docker/postgis
https://github.com/openmaptiles/openmaptiles-tools/blob/master/docker/postgis/Dockerfile


docker run \
    -v $(pwd)/data/postgis:/var/lib/postgresql/data \
    -e POSTGRES_DB="openmaptiles" \
    -e POSTGRES_USER="openmaptiles" \
    -e POSTGRES_PASSWORD="openmaptiles" \
    -d openmaptiles/postgis



docker-compose -f docker-compose.tile-server.yml run --rm osm-toolbox bash

psql -U openmaptiles -W


[XML Config for Mapnik](https://github.com/mapnik/mapnik/wiki/PostGIS)


TODO: put this behind kong
Original tile server: https://a.tile.openstreetmap.org
httpd/tiles/renderd-example

`docker network connect claris-suite_claris-network alchemy-map-prototype-httpd-1 --alias httpd`

# Steps
1. Create database with initial dataset
    * maybe that is planet, maybe that is one state
2. Import shape files (get-external data)
3. generate mapnik.xml maybe?

`psql -U openmaptiles -d openmaptiles -a -f drop-tables.sql`
`./import-osm-to-postgres.sh --file /data/merged.osm.pbf --create`
`psql -h postgis -U openmaptiles -W -d openmaptiles -a -f indexes.sql`

`osm2pgsql-replication init -d openmaptiles -H postgis -U openmaptiles --osm-file /data/maryland.osm.pbf`

```
root@50bab5011dbc:/app# osm2pgsql-replication status -d openmaptiles -H postgis -U openmaptiles
Using replication service 'http://download.geofabrik.de/north-america/us/maryland-updates', which is at sequence 3942 ( 2024-01-14T21:21:29Z )
Replication server's most recent data is 23 hour(s) 28 minute(s) old
Local database is 2 sequences behind the server, i.e. 2 day(s) 0 hour(s) 0 minute(s)
Local database's most recent data is 2 day(s) 23 hour(s) 28 minute(s) old
```

used the following to clear tiles, I should be able to use `render_expoired` to do this.... but I don't know how to use it yet.
`rm -rf /var/cache/renderd/tiles/example-map/`